import { Tags } from 'hot-shots';
import send from './client/send';

const gauge = (suffix: string, value: number, tags?: Tags): void => {
  send('gauge', suffix, value, tags);
};

export default gauge;
