import { Tags } from 'hot-shots';
import send from './client/send';

const histogram = (suffix: string, value: number, tags?: Tags): void => {
  send('histogram', suffix, value, tags);
};

export default histogram;
