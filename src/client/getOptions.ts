import { ClientOptions } from 'hot-shots';

const getOptions = (): ClientOptions => {
  const hostEnvVar = process.env.DATADOG_HOST_ENVVAR || 'DATADOG_HOST';

  return {
    host: process.env[hostEnvVar] || '127.0.0.1',
    port: parseInt(process.env.DATADOG_PORT || '8125', 10),
    mock: process.env.DATADOG_MOCK === 'true',
  };
};

export default getOptions;
