import { Tags } from 'hot-shots';
import { error } from '@mvf/logger';
import { map } from 'lodash';
import { getClient } from './client';

const getAllTags = (tags?: Tags): string[] => {
  const serviceName = process.env.DATADOG_SERVICE_NAME || 'notset';
  const tagArray = [`service:${serviceName}`];
  if (Array.isArray(tags)) {
    tagArray.push(...tags);
  } else {
    tagArray.push(
      ...map(tags, (value, key) =>
        typeof value === 'string' ? `${key}:${value}` : key,
      ),
    );
  }

  return tagArray;
};

type DatadogMethod =
  | 'increment'
  | 'decrement'
  | 'histogram'
  | 'gauge'
  | 'unique'
  | 'timing';

const send = (
  method: DatadogMethod,
  suffix: string,
  value: number,
  tags?: Tags,
): void => {
  try {
    const projectName = process.env.DATADOG_PROJECT_NAME || 'notset';
    getClient()[method](
      projectName.concat('.', suffix),
      value,
      getAllTags(tags),
    );
  } catch (err) {
    if (err instanceof Error) {
      error(err.message);
    } else {
      error('Unknown error encountered while sending statsd data', {
        method,
        suffix,
        value,
        tags,
      });
    }
  }
};

export default send;
