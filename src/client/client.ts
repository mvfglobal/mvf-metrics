import { StatsD } from 'hot-shots';
import { error } from '@mvf/logger';
import getOptions from './getOptions';

let client: StatsD;

export const getClient = (): StatsD => {
  if (!client) {
    client = new StatsD(getOptions());
  }

  return client;
};

export const disconnect = (): void =>
  getClient().close((err) => error(err?.message));

export const getMockBuffer = (): string[] | undefined => getClient().mockBuffer;
