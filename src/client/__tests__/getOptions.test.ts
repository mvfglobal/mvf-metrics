import getOptions from '../getOptions';

const OLD_ENV = process.env;

beforeEach(() => {
  process.env = { ...OLD_ENV };
  process.env.DATADOG_MOCK = 'true';
});

afterEach(() => {
  process.env = OLD_ENV;
});

it('should set the host using the DATADOG_HOST_ENVVAR if set', () => {
  process.env.DATADOG_HOST_ENVVAR = 'VAR';
  process.env.VAR = 'HOST';
  expect(getOptions()).toHaveProperty('host', 'HOST');
});

it('should set the host using the DATADOG_HOST variable if DATADOG_HOST_ENVVAR is not set', () => {
  process.env.DATADOG_HOST = 'HOST';
  expect(getOptions()).toHaveProperty('host', 'HOST');
});

it('should set the host to localhost if no host is set', () => {
  expect(getOptions()).toHaveProperty('host', '127.0.0.1');
});

it('should set the port if DATADOG_PORT is set', () => {
  process.env.DATADOG_PORT = '1234';
  expect(getOptions()).toHaveProperty('port', 1234);
});

it('should set the port to default if no port is set', () => {
  expect(getOptions()).toHaveProperty('port', 8125);
});

it('should set mock to true if set as true', () => {
  expect(getOptions()).toHaveProperty('mock', true);
});

it('should set mock to false if set as false', () => {
  process.env.DATADOG_MOCK = 'false';
  expect(getOptions()).toHaveProperty('mock', false);
});
