import { StatsD } from 'hot-shots';
import send from '../send';

const OLD_ENV = process.env;

const incrementSpy = jest.spyOn(StatsD.prototype, 'increment');

beforeEach(() => {
  jest.resetAllMocks();
  process.env = { ...OLD_ENV };
  process.env.DATADOG_MOCK = 'true';
});

afterEach(() => {
  process.env = OLD_ENV;
});

it("should set the project name to 'notset' if it is not set", () => {
  send('increment', 'test', 1);
  expect(incrementSpy).toHaveBeenCalledWith(
    expect.stringContaining('notset'),
    expect.any(Number),
    expect.any(Array),
  );
});

it('should set the project name if DATADOG_PROJECT_NAME is set', () => {
  process.env.DATADOG_PROJECT_NAME = 'project';
  send('increment', 'test', 1);
  expect(incrementSpy).toHaveBeenCalledWith(
    expect.stringContaining('project'),
    expect.any(Number),
    expect.any(Array),
  );
});

it("should set the service name to 'notset' if it is not set", () => {
  send('increment', 'test', 1);
  expect(incrementSpy).toHaveBeenCalledWith(
    expect.any(String),
    expect.any(Number),
    expect.arrayContaining(['service:notset']),
  );
});

it('should set the service name if DATADOG_SERVICE_NAME is set', () => {
  process.env.DATADOG_SERVICE_NAME = 'service';
  send('increment', 'test', 1);
  expect(incrementSpy).toHaveBeenCalledWith(
    expect.any(String),
    expect.any(Number),
    expect.arrayContaining(['service:service']),
  );
});
