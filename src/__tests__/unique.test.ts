import { StatsD } from 'hot-shots';
import * as Logger from '@mvf/logger/client';
import unique from '../unique';

const OLD_ENV = process.env;

const uniqueSpy = jest.spyOn(StatsD.prototype, 'unique');
const errorSpy = jest.spyOn(Logger, 'error');

beforeEach(() => {
  process.env = { ...OLD_ENV };
  process.env.DATADOG_MOCK = 'true';
  process.env.DATADOG_PROJECT_NAME = 'test_project';
  process.env.DATADOG_SERVICE_NAME = 'test_service';
});

afterEach(() => {
  process.env = OLD_ENV;
});

it('should call the client increment method with the correct arguments', () => {
  unique('suffix', 1, { tag: 'value' });
  expect(uniqueSpy).toHaveBeenCalledWith('test_project.suffix', 1, [
    'service:test_service',
    'tag:value',
  ]);
});

it('should log an error one if one is thrown when sending the stats', () => {
  uniqueSpy.mockImplementationOnce(() => {
    throw new Error();
  });
  unique('suffix', 1, { tag: 'value' });
  expect(errorSpy).toHaveBeenCalledTimes(1);
});
