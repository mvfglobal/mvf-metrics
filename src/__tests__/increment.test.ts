import { StatsD } from 'hot-shots';
import * as Logger from '@mvf/logger/client';
import increment from '../increment';

const OLD_ENV = process.env;

const incrementSpy = jest.spyOn(StatsD.prototype, 'increment');
const errorSpy = jest.spyOn(Logger, 'error');

beforeEach(() => {
  process.env = { ...OLD_ENV };
  process.env.DATADOG_MOCK = 'true';
  process.env.DATADOG_PROJECT_NAME = 'test_project';
  process.env.DATADOG_SERVICE_NAME = 'test_service';
});

afterEach(() => {
  process.env = OLD_ENV;
});

it('should call the client increment method with the correct arguments', () => {
  increment('suffix', 1, { tag: 'value' });
  expect(incrementSpy).toHaveBeenCalledWith('test_project.suffix', 1, [
    'service:test_service',
    'tag:value',
  ]);
});

it('should log an error one if one is thrown when sending the stats', () => {
  incrementSpy.mockImplementationOnce(() => {
    throw new Error();
  });
  increment('suffix', 1, { tag: 'value' });
  expect(errorSpy).toHaveBeenCalledTimes(1);
});
