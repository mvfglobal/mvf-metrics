import { StatsD } from 'hot-shots';
import * as Logger from '@mvf/logger/client';
import time from '../time';

const OLD_ENV = process.env;

const timeSpy = jest.spyOn(StatsD.prototype, 'timing');
const errorSpy = jest.spyOn(Logger, 'error');

beforeEach(() => {
  jest.resetAllMocks();
  process.env = { ...OLD_ENV };
  process.env.DATADOG_MOCK = 'true';
  process.env.DATADOG_PROJECT_NAME = 'test_project';
  process.env.DATADOG_SERVICE_NAME = 'test_service';
});

afterEach(() => {
  process.env = OLD_ENV;
});

it('should call the client timing method with 1 for the logged seconds', () => {
  time('suffix', 1000, { tag: 'value' });
  expect(timeSpy).toHaveBeenCalledWith('test_project.suffix', 1, [
    'service:test_service',
    'tag:value',
  ]);
});

it('should call the client timing method with the correct arguments', () => {
  time('suffix', () => {}, { tag: 'value' });
  expect(timeSpy).toHaveBeenCalledWith(
    'test_project.suffix',
    expect.any(Number),
    ['service:test_service', 'tag:value'],
  );
});

it('should log an error one if one is thrown when sending the stats', () => {
  timeSpy.mockImplementationOnce(() => {
    throw new Error();
  });
  time('suffix', () => {}, { tag: 'value' });
  expect(errorSpy).toHaveBeenCalledTimes(1);
});

it('should call the client timing method with the correct arguments for an asynchronous function', async () => {
  await time('suffix', async () => {}, {
    tag: 'value',
  });
  expect(timeSpy).toHaveBeenCalledWith(
    'test_project.suffix',
    expect.any(Number),
    ['service:test_service', 'tag:value'],
  );
});

it('should log an error one if one is thrown when sending the stats', async () => {
  timeSpy.mockImplementationOnce(() => {
    throw new Error();
  });
  await time('suffix', async () => {}, {
    tag: 'value',
  });
  expect(errorSpy).toHaveBeenCalledTimes(1);
});

it('should return the result of callback', () => {
  const result = time('suffix', () => 'callback result', { tag: 'value' });
  expect(result).toEqual('callback result');
});

it('should return the result of async callback', async () => {
  // eslint-disable-next-line @typescript-eslint/require-await
  const result = await time('suffix', async () => 'async callback result', {
    tag: 'value',
  });
  expect(result).toEqual('async callback result');
});
