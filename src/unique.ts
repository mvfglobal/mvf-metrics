import { Tags } from 'hot-shots';
import send from './client/send';

const unique = (suffix: string, value: number, tags?: Tags): void => {
  send('unique', suffix, value, tags);
};

export default unique;
