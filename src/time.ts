import { Tags } from 'hot-shots';
import send from './client/send';

const syncTime = (
  timeStart: number,
  timeEnd: number,
  suffix: string,
  tags?: Tags,
): void => {
  const milliseconds = timeEnd - timeStart;
  const seconds = parseFloat(milliseconds.toFixed(3)) / 1000;
  send('timing', suffix, seconds, tags);
};

const asyncTime = async <T>(
  timeStart: number,
  suffix: string,
  promise: Promise<T>,
  tags?: Tags,
): Promise<T> => {
  const promiseResult = await promise;
  const timeEnd = Date.now();

  syncTime(timeStart, timeEnd, suffix, tags);

  return promiseResult;
};

function time(suffix: string, milliseconds: number, tags?: Tags): void;
function time<T = void>(
  suffix: string,
  asyncCallback: () => Promise<T>,
  tags?: Tags,
): Promise<T>;
function time<T = void>(suffix: string, callback: () => T, tags?: Tags): T;
function time<T = void>(
  suffix: string,
  millisecondsOrCallback: number | (() => T) | (() => Promise<T>),
  tags?: Tags,
): unknown {
  if (typeof millisecondsOrCallback === 'number') {
    return syncTime(0, millisecondsOrCallback, suffix, tags);
  }

  const callback = millisecondsOrCallback;

  const timeStart = Date.now();
  const resultOrPromise = callback();
  const timeEnd = Date.now();
  if (resultOrPromise instanceof Promise) {
    const promise = resultOrPromise;

    return asyncTime(timeStart, suffix, promise, tags);
  }

  const result = resultOrPromise;
  syncTime(timeStart, timeEnd, suffix, tags);

  return result;
}

export default time;
