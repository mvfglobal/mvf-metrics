import { Tags } from 'hot-shots';
import send from './client/send';

const decrement = (suffix: string, value: number, tags?: Tags): void => {
  send('decrement', suffix, value, tags);
};

export default decrement;
