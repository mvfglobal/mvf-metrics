import { Tags } from 'hot-shots';
import send from './client/send';

const increment = (suffix: string, value: number, tags?: Tags): void => {
  send('increment', suffix, value, tags);
};

export default increment;
