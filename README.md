> Migrated to the monorepo https://bitbucket.org/mvfglobal/rhino-monorepo/src/master/packages/metrics/

# MVF Metrics

## Usage

Provides a consistent way to log metrics in javascript applications.

### To install the package

Run `npm install @mvf/metrics`

### Configuration

Set the following environment variables in your project

- `DATADOG_HOST` or `DATADOG_HOST_ENVVAR` if the datadog host is stored in another environment variable, for example, if datadog host is assigned to `HOST` env variable then you can set `DATADOG_HOST_ENVVAR=HOST` which will then load datadog host from `HOST` environment variable.
- `DATADOG_PORT` or defaults to `8125` if not set.
- `DATADOG_MOCK` should be set to true in development and testing environments. This will setup [hot-shot](https://www.npmjs.com/package/hot-shots) Client in a mock mode.
- `DATADOG_PROJECT_NAME`
- `DATADOG_SERVICE_NAME`

### More on DATADOG_PROJECT_NAME

This is the system name of your project, e.g. abbey, mercury, leads, etc. `DATADOG_PROJECT_NAME` is used as the default prefix of your metric name, for example if `DATADOG_PROJECT_NAME` is set to

```env
DATADOG_PROJECT_NAME=abbey
```

in your environment and you run

```ts
histogram('my.metric', 5);
```

or any other metric function then the full metric name sent to datadog will be `abbey.my.metric`.

### More on DATADOG_SERVICE_NAME

This is the type of service in your project, e.g. web, api, cron, consumer, worker, etc. `DATADOG_SERVICE_NAME` is attached to every metric as a tag and should be used to group metrics by the type of service, for example if `DATADOG_SERVICE_NAME` is set to

```env
DATADOG_SERVICE_NAME=cron
```

in your environment and you run

```ts
histogram('my.metric', 5);
```

or any other metric function then that metric will have `service:cron` tag.

### Decrement
To decrement `index` metric by `5` use the following
```ts
import { decrement } from '@mvf/metrics';

decrement('index', 5);
```

### Gauge
To gauge `index` metric to `50` use the following
```ts
import { gauge } from '@mvf/metrics';

gauge('index', 5);
```

### Histogram
To gauge `index` metric to `50` use the following
```ts
import { histogram } from '@mvf/metrics';

histogram('index', 5);
```

### Increment
To increment `index` metric by `5` use the following
```ts
import { increment } from '@mvf/metrics';

increment('index', 5);
```

### Time
This is used to time how long something took to run. There are 3 ways this function can be used.

#### Call with explicit value

To log `1000` milliseconds for `calculateFavorites` metric use the following
```ts
import { time } from '@mvf/metrics';

const milliseconds = 1000;
time('calculateFavorites', milliseconds);
```

#### Call with callback

To count how long something takes to run and log that time use the callback approach
```ts
import { time } from '@mvf/metrics';

const calculateFavorites = () => {
  // do something
};

time('calculateFavorites', calculateFavorites);
```

#### Call with async callback

If your callback is asynchronous you can use async function and await
```ts
import { time } from '@mvf/metrics';

const calculateFavorites = async () => {
  // do something
};

await time('calculateFavorites', calculateFavorites);
```

#### Return value from callback

Any value returned from the callback will be return by the time function, e.g.

`sync callback`

```ts
const data = time('calculateFavorites', () => 'data');
```

`async callback`

```ts
const data = await time('calculateFavorites', async () => 'data');
```

### Unique
To log `50` for `currentIndex` metric use the following
```ts
import { unique } from '@mvf/metrics';

unique('currentIndex', 50);
```

## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
